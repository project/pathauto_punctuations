<?php

namespace Drupal\pathauto_punctuations\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Pathauto Punctuations settings for this site.
 */
class PathautoPunctuationsAdmin extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pathauto_punctuations_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['pathauto_punctuations.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('pathauto_punctuations.settings')->get('punctuations');
    $punctuations = $config['group'];

    $qty = $form_state->get('qty') ? $form_state->get('qty') : count($punctuations);
    if ($qty === NULL) {
      $qty = 0;
    }

    $form_state->set('qty', $qty);

    $form['description'] = [
      '#type' => 'item',
      '#markup' => $this->t('Enter the name and character you want to replace.'),
    ];

    $form['#tree'] = TRUE;

    $form['fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Name and Character'),
      '#prefix' => '<div id="names-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];

    for ($i = 0; $i < $qty; $i++) {
      $form['fieldset']['group'][$i] = [
        '#tree' => TRUE,
      ];

      $form['fieldset']['group'][$i]['name'][$i] = [
        '#type' => 'textfield',
        '#title' => $this->t('Name'),
        '#default_value' => $punctuations[$i]['name'][$i],
      ];

      $form['fieldset']['group'][$i]['char'][$i] = [
        '#type' => 'textfield',
        '#title' => $this->t('Character'),
        '#default_value' => $punctuations[$i]['char'][$i],
      ];
    }

    $form['fieldset']['actions'] = [
      '#type' => 'actions',
    ];

    $form['fieldset']['actions']['add_name'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add one more'),
      '#submit' => ['::addOne'],
      '#ajax' => [
        'callback' => '::addmoreCallback',
        'wrapper' => 'names-fieldset-wrapper',
      ],
    ];

    if ($qty > 1) {
      $form['fieldset']['actions']['remove_name'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove one'),
        '#submit' => ['::removeCallback'],
        '#ajax' => [
          'callback' => '::addmoreCallback',
          'wrapper' => 'names-fieldset-wrapper',
        ],
      ];
    }

    $form['slugify-fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Slugify'),
      '#prefix' => '<div id="slugify-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];

    $form['slugify-fieldset']['to-slugify'] = [
      '#type' => 'textfield',
      '#title' => $this->t('String to slugify'),
      '#default_value' => "",
    ];

    $form['slugify-fieldset']['slugified'] = [
      '#type' => 'textfield',
      '#title' => $this->t('slugified'),
      '#default_value' => "",
    ];

    $form['slugify-fieldset']['actions']['slugify'] = [
      '#type' => 'submit',
      '#value' => $this->t('Slugify'),
      '#submit' => ['::slugify'],
      '#ajax' => [
        'callback' => '::slugifyCallback',
        'wrapper' => 'slugify-fieldset-wrapper',
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function slugify(array &$form, FormStateInterface $form_state) {
    $to_slugify = $form_state->getValue(['slugify-fieldset', 'to-slugify']);
    $clean_string = \Drupal::service('pathauto.alias_cleaner')->cleanString($to_slugify);
    $form['slugify-fieldset']['slugified']['#value'] = $clean_string;
  }

  /**
   * {@inheritdoc}
   */
  public function slugifyCallback(array &$form, FormStateInterface $form_state) {
    return $form['slugify-fieldset'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue(['fieldset']);
    $this->config('pathauto_punctuations.settings')
      ->set('punctuations', $values)
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the fieldset with the names in it.
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['fieldset'];
  }

  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $qty = $form_state->get('qty');
    $add_button = $qty + 1;
    $form_state->set('qty', $add_button);
    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "remove one" button.
   *
   * Decrements the max counter and causes a form rebuild.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $qty = $form_state->get('qty');
    if ($qty > 1) {
      $remove_button = $qty - 1;
      $form_state->set('qty', $remove_button);
    }
    $form_state->setRebuild();
  }

}
